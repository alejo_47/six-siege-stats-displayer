var raiseAlert = (id, timeout) => {
	$('#' + id).removeClass('hidden').addClass('shown');
	setTimeout(() => {
		$('.submit').prop('disabled', false)
		$('#' + id).addClass('hidden').removeClass('shown');
	}, timeout * 1000);
};

var loadUsers = (users) => {
	let form = $('form')[0]
	$('#slots').on('change', (event) => {
		let slot = event.target.value
		form.username.value = users[slot].name;
		form.region.value = users[slot].region || 'ncsa';
		form.platform.value = users[slot].platform || 'uplay';
	})
}

Twitch.ext.onAuthorized((auth) => {
	$(document).ready(() => {
		let form = $('form')[0]
	 	$.ajax({
			url: `https://ext.alejo47.com/user/${auth.channelId}`,
			method: 'GET',
			success: (d) => {
				form.username.value = d.users[0].name;
				form.region.value = d.users[0].region;
				form.platform.value = d.users[0].platform;
				form.slot.value = 0;
				loadUsers(d.users)
			}
	 	})

		$('form').on('submit', (event) => {
			event.preventDefault();
			$('.submit').prop('disabled', true)
			form = $('form')[0];
			let fd = JSON.stringify({
				username: form.username.value,
				region: form.region.value,
				platform: form.platform.value,
				accSlot: form.slot.value,
				jwt: auth.token
			})
			$.ajax({
				url: `https://ext.alejo47.com/user/${auth.channelId}`,
				method: 'POST',
				contentType: 'application/json',
				data: fd,
				success: (result) => {
					raiseAlert('success', 2);
				},
				error: (result) => {
					console.log(result);
					$('#error_message')[0].innerText = result.responseJSON['message']
					raiseAlert('error', 2);
				}
			});
		});
	});
});

Twitch.ext.onContext((c) => {
	$(document).ready(() => {
		if (c.theme == 'light') {
			$('body').addClass('light').removeClass('dark')
		}
		else if (c.theme == 'dark') {
			$('body').addClass('dark').removeClass('light')
		}
	});
});
