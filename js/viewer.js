const RANKS = [ 
'Unranked',
'Copper IV', 'Copper III', 'Copper II', 'Copper I',
'Bronze IV', 'Bronze III', 'Bronze II', 'Bronze I',
'Silver IV', 'Silver III', 'Silver II', 'Silver I',
'Gold IV', 'Gold III', 'Gold II', 'Gold I',
'Plat III', 'Plat II', 'Plat I',
'Diamond I']

var startLoadingScreen = () => {
	$('div#stats').html(`<div id='loading'><div class='dot-0 dot'></div><div class='dot-1 dot'></div><div class='dot-2 dot'></div><div class='dot-3 dot'></div><div class='dot-4 dot'></div></div>`)
}

var getUserData = (user, callback) => {
	$.ajax({
		url: `https://ext.alejo47.com/stats/${user.id}/${user.platform}/${user.region}`,
		success: callback,
		error: (_, status, error) => {return { 'error': error, 'status': status}}
	});
}

var operatorToHTML = (operator) => {
	return `<div class='operator operator-${operator.opRole}'>
	<span class='operator-header'>${operator.opName}</span><br>
	<img class='operator-logo' src='https://alejo47.com/cdn/siege/operator_logos/${operator.opName.toLowerCase()}.png'>
	</div>`
}

var playerToHTML = (user, callback) => {
	getUserData(user, (player) => {
		let operators = player.stats.operators
		let mostPlayedDef = operators.filter(op => op.opRole == 'def')[0]
		let mostPlayedAtk = operators.filter(op => op.opRole == 'atk')[0]

		let html = `<div class='player' data-username='${user.name}' data-platform='${user.platform}'>`
		html += `<img src='https://ubisoft-avatars.akamaized.net/${user.id}/default_256_256.png' class='user-avatar'><span class='username-header'>${user.name}</span>`

		if (player.ranked.wins + player.ranked.losses == 0 || player.ranked.rank == 0) {
				html += `<div class='player-rank'><img class='rank-emblem' src='https://alejo47.com/cdn/siege/rank_emblems/unranked.svg'><br>Unranked</div>`
		}
		else {
				html += `<div class='player-rank'><img class='rank-emblem' src='https://alejo47.com/cdn/siege/rank_emblems/${RANKS[player.ranked.rank].toLocaleLowerCase()}.svg'><br>${RANKS[player.ranked.rank]} @ ${Math.floor(player.ranked.mmr)} MMR</div>`
		}
		html += `<div class='player-playtime'>Level ${player.accountInfo.level} | <i class='far fa-clock'></i> ${Math.round((player.stats.casual.timeplayed + player.stats.ranked.timeplayed) / 60 / 60)}hs played</div><br><div class='player-operators'><p class='operators-header'>Most played operators</p><br>${operatorToHTML(mostPlayedAtk)}${operatorToHTML(mostPlayedDef)}</div></div>`
		callback(html);
	});
}

Twitch.ext.onAuthorized((auth) => {
	$(document).ready(() => {
		startLoadingScreen();
		$.ajax({
			url: `https://ext.alejo47.com/user/${auth.channelId}`,
			success: (response) => {
				let slot = 0;
				users = response.users.filter((u, i) => {return u.id != null;})
				$('#next').on('click', () => {
					slot + 1 > (users.length-1) ? slot = 0 : slot += 1;
					startLoadingScreen();
					playerToHTML(users[slot], (player) => {
						$('div#stats').html(player)
					});
				})
				$('#previous').on('click', () =>{
					slot - 1 < 0 ? slot = (users.length-1) : slot -= 1;
					startLoadingScreen();
					playerToHTML(users[slot], (player) => {
						$('div#stats').html(player)
					});
				})
				
				playerToHTML(users[slot], (player) => {
					$('div#stats').html(player)
					if (users.length > 1) { $('i.float').removeClass('hidden')}
				});
				setInterval(() => {
						playerToHTML(users[slot], (player) => {
						$('div#stats').html(player)
					});
				}, 10 * 60 * 1000)
			},
			error: () => {
				$('#error').removeClass('hidden').addClass('shown');
				setTimeout(() => {
					$('#error').addClass('hidden').removeClass('shown');
				}, 10 * 1000);
			}
		});
	});
});

Twitch.ext.onContext((c) => {
	$(document).ready(() => {
		if (c.theme == 'light') {
			$('body').addClass('light').removeClass('dark')
		}
		else if (c.theme == 'dark') {
			$('body').addClass('dark').removeClass('light')
		}
	});
});
